# Changelog
* All changes since v0.1.11

## [0.1.15] 21-01-2020
### Removed
    - remove dependency exception and random-faster
    - add Error.cs and RandomFaster.cs as part of Utility

## [0.1.14] 21-01-2020
### Removed
	- remove assembly editor

## [0.1.13] 15-01-2020
### Added
    -Add ColorCollection.cs follow `https://www.w3.org/TR/css-color-3/#svg-color`


## [0.1.9] - 31-12-2019
### Add extendsion method `ToAlphabet` for `BigInteger`
	- formatting Big Numbers: The “aa” Notation
        
         number                alphabet
         1                        1
         1000                     1K
         1000000                  1M
         1000000000               1B
         1000000000000            1T
         1000000000000000         1AA

```csharp
    BigInteger integer = BigInteger.Parse("100000000000000");
    Debug.Log(integer.ToAlphabet());
```

