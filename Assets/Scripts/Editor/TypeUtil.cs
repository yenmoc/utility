﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UnityModule.EditorUtility
{
    public static class TypeUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static bool Exist(string assemblyName)
        {
            return AppDomain.CurrentDomain
                       .GetAssemblies()
                       .FirstOrDefault(assembly => assembly.GetName().Name.Equals(assemblyName)) != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public static Type GetTypeByName(string className)
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .FirstOrDefault(type => type.Name == className);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="className"></param>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static Type GetTypeByName(string className, string assemblyName)
        {
            return Assembly
                .Load(assemblyName)
                .GetTypes()
                .FirstOrDefault(type => type.Name == className);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string[] GetAllImplementFormType<T>() where T : class
        {
            var allImplements = new List<string>();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var types = assembly.GetTypes()
                    .Where(t => t != typeof(T))
                    .Where(t => typeof(T).IsAssignableFrom(t))
                    .Select(t => t.Name);
                allImplements.AddRange(types);
            }

            return allImplements.ToArray();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string[] GetAllImplementFormType<T>(string assemblyName) where T : class
        {
            var allImplements = new List<string>();
            var types = Assembly.Load(assemblyName)
                .GetTypes()
                .Where(t => t != typeof(T))
                .Where(t => typeof(T).IsAssignableFrom(t))
                .Select(t => t.Name);
            allImplements.AddRange(types);
            return allImplements.ToArray();
        }
    }
}
#endif