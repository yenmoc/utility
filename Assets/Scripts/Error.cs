﻿using System;

// ReSharper disable UnusedMember.Global
// ReSharper disable CheckNamespace
namespace UnityModule.Ex
{
    public static class Error
    {
        public static Exception ArgumentNull(string argumentName, string message = "") { return new ArgumentNullException(argumentName, message); }

        public static Exception ArgumentOutOfRange(string argumentName) { return new ArgumentOutOfRangeException(argumentName); }

        public static Exception MoreThanOneElement() { return new InvalidOperationException("Sequence contains more than one element"); }

        public static Exception MoreThanOneMatch() { return new InvalidOperationException("Sequence contains more than one matching element"); }

        public static Exception NoElements() { return new InvalidOperationException("Sequence contains no elements"); }

        public static Exception NoMatch() { return new InvalidOperationException("Sequence contains no matching element"); }

        public static Exception NotSupported() { return new NotSupportedException(); }
        
    }
}